# UAS Prak.Basis Data

## Aplikasi
[Agoda]

## No.1

## Use case diagram

| No | Aktivitas | Skala Prioritas | User / Admin |
| -- | ------ | ------ | ------ |
|  1 | Pencarian Akomodasi                                                           | Tinggi 85%                | User          |
|  2 | Reservasi Akomodasi                                                           | Tinggi 90%                | User          |
|  3 | Pembayaran                                                                    | Tinggi 95%                | User          |
|  4 | Kelola Data Pemesanan                                                         | Tinggi 80%                | Admin         |
|  5 | Kelola Data Akomodasi                                                         | Tinggi 80%                | Admin         |
|  6 | Verifikasi Pembayaran                                                         | Sedang 75%                | User          |
|  7 | Kelola Data Pengguna                                                          | Tinggi 80%                | Admin         |
|  8 | Notifikasi Reservasi                                                          | Sedang 70%                | User          |
|  9 | Pencarian Berdasarkan Kategori                                                | Sedang 70%                | User          |
| 10 | Ulasan dan Penilaian Akomodasi                                                | Sedang 75%                | User          |
| 11 | Penanganan Keluhan Pelanggan                                                  | Tinggi 80%                | Admin         |
| 12 | Integrasi Dengan Sistem Pembayaran                                            | Tinggi 85%                | Admin         |
| 13 | Kelola Ketersediaan Akomodasi                                                 | Tinggi 90%                | Admin         |
| 14 | Pengelolaan Promosi                                                           | Tinggi 85%                | Admin         |
| 15 | Pemesanan Layanan Tambahan                                                    | Sedang 70%                | User          |
| 16 | Sistem Poin Atau Reward                                                       | Sedang 70%                | User          |
| 17 | Pencarian Akomodasi Berdasarkan Harga                                         | Tinggi 80%                | User          |
| 18 | Penjadwalan Ulang Pemesanan                                                   | Sedang 75%                | User          |
| 19 | Sistem Notifikasi Harga Terbaik                                               | Sedang 70%                | User          |
| 20 | Integrasi dengan peta dan navigasi                                            | Sedang 70%                | User          |
| 21 | Pencarian Akomodasi Berdasarkan Fasilitas                                     | Sedang 70%                | User          |
| 22 | Kelola Ulasan Pengguna                                                        | Sedang 70%                | Admin         |
| 23 | Penjadwalan Pengingat Reservasi                                               | Rendah 60%                | User          |
| 24 | Pencarian Akomodasi Dengan Aksesibilitas                                      | Sedang 70%                | User          |
| 25 | Integrasi Dengan Sistem Transportasi                                          | Tinggi 80%                | User          |
| 26 | Layanan Pelanggan 24/7                                                        | Tinggi 85%                | User          |
| 27 | Integrasi Dengan Platform Perjalanan Lainnya                                  | Tinggi 85%                | User          |
| 28 | Kelola Kebijakan Pembatalan                                                   | Tinggi 90%                | Admin         |
| 29 | Integrasi Dengan Sistem Loyalty Program                                       | Sedang 75%                | User          |
| 30 | Pencarian Akomodasi Berdasarkan Aktivitas                                     | Sedang 70%                | User          |
| 31 | Pemberitahuan Perubahan Harga                                                 | Sedang 75%                | User          |
| 32 | Kelola Data Mitra Akomodasi                                                   | Sedang 65%                | Admin         |
| 33 | Fitur Wishlist Atau Daftar Favorit                                            | Sedang 70%                | User          |
| 34 | Sistem Rekomendasi Akomodasi                                                  | Sedang 75%                | User          |
| 35 | Kelola Penawaran Spesial Dan Diskon                                           | Tinggi 80%                | Admin         |
| 36 | Fitur Akomodasi Berdasarkan Tanggal                                           | Sedang 75%                | User          |
| 37 | Kelola Data Tagihan Dan Faktur                                                | Tinggi 85%                | Admin         |
| 38 | Fitur Perbandingan Harga Dengan Pesaing                                       | Sedang 75%                | User          |
| 39 | Kelola Program Aflliasi                                                       | Tinggi 80%                | Admin         |
| 40 | Fitur Pencarian Akomodasi Dekat Landmark                                      | Sedang 70%                | User          |
| 41 | Fitur Penyimpanan Preferensi Pengguna                                         | Sedang 70%                | User          |
| 42 | Kelola Laporan Dan Analisis Data                                              | Tinggi 85%                | Admin         |
| 43 | Pemesanan Akomodasi Dengan Voucher                                            | Tinggi 80%                | User          |
| 44 | Kelola Data Lokasi Akomodasi                                                  | Sedang 75%                | Admin         |
| 45 | Fitur Pemesanan Akomodasi Untuk Acara Khusus                                  | Tinggi 85%                | User          |
| 46 | Kelola Program Kemitraan                                                      | Tinggi 80%                | Admin         |
| 47 | Fitur Pemesanan Dengan Pembayaran Cicilan                                     | Tinggi 80%                | User          |
| 48 | Kelola Data Tarif Dan Harga Akomodasi                                         | Tinggi 85%                | Admin         |
| 49 | Fitur Rekomendasi Berdasarkan Riwayat Pemesanan                               | Sedang 75%                | User          |
| 50 | Fitur Notifikasi Perubahan Harga                                              | Sedang 70%                | User          |
| 51 | Kelola Data Ulasan Dan Penilaian Pengguna                                     | Sedang 75%                | Admin         |
| 52 | Kelola Program Reward Dan Loyalitas                                           | Tinggi 80%                | Admin         |
| 53 | Kelola Data Kebijakan Pembayaran                                              | Tinggi 85%                | Admin         |
| 54 | Kelola Data Kebijakan Pembatalan Akomodasi                                    | Tinggi 85%                | Admin         |
| 55 | Kelola Data Promo Dan Diskon                                                  | Tinggi 85%                | Admin         |
| 56 | Fitur Pemesanan Akomodasi Dengan Opsi Makanan                                 | Sedang 75%                | User          |
| 57 | Kelola Data Kebijakan Privasi Dan Keamanan                                    | Tinggi 80%                | Admin         |
| 58 | Kelola Data Kebijakan Pengembalian Dana                                       | Tinggi 80%                | Admin         |
| 59 | Kelola Data Kebijakan Pembayaran Khusus                                       | Tinggi 85%                | Admin         |
| 60 | Kelola Data Kebijakan Pemesanan Khusus                                        | Tinggi 80%                | Admin         |
| 61 | Kelola Data Kebijakan Penghapusan Data Pengguna                               | Tinggi 80%                | Admin         |
| 62 | Kelola Data Kebijakan Keamanan Akun Pengguna                                  | Tinggi 80%                | Admin         |
| 63 | Kelola Data Kebijakan Keamanan Pembayaran                                     | Tinggi 80%                | Admin         |
| 64 | Kelola Data Kebijakan Privasi Pengguna                                        | Tinggi 80%                | Admin         |
| 65 | Fitur Pemesanan Akomodasi Dengan Layanan 24 Jam                               | Sedang 75%                | User          |
| 66 | Kelola Data Kebijakan Pembatalan Dan Pengembalian Dana                        | Tinggi 80%                | Admin         |
| 67 | Kelola Data Kebijakan Penggunaan Poin Reward                                  | Tinggi 80%                | Admin         |
| 68 | Kelola Data Kebijakan Pengguna Kupon Diskon                                   | Tinggi 80%                | Admin         |
| 69 | Pencarian Akomodasi Dekat Pusat Kota                                          | Sedang 75%                | User          |
| 70 | Fitur Pemesanan Akomodasi Dengan Layanan Antar Makanan                        | Sedang 75%                | User          |
| 71 | Pencarian Akomodasi Dengan Akses Ke Tempat Wisata                             | Sedang 70%                | User          |
| 72 | Pencarian Akomodasi Dengan Jarak Terdekat ke Bandara                          | Sedang 75%                | User          |
| 73 | Pencarian Akomodasi Dengan Jarak Terdekat Ke Transportasi Umum                | Sedang 75%                | User          |
| 74 | Pencarian Akomodasi Dengan Jarak Terdekat Ke Landmark                         | Sedang 75%                | User          |
| 75 | Pencarian penerbangan berdasarkan lokasi keberangkatan dan tujuan             | Tinggi 90%                | User          |
| 76 | Pilihan penerbangan dengan filter waktu, harga, maskapai, dll.                | Sedang 80%                | User          |
| 77 | Melihat detail penerbangan termasuk jadwal, durasi, dan fasilitas             | Sedang 75%                | User          |
| 78 | Memilih kursi atau kelas penerbangan                                          | Sedang 75%                | User          |
| 79 | Memasukkan informasi penumpang, seperti nama dan tanggal lahir                | Tinggi 80%                | User          |
| 80 | Melakukan pembayaran untuk pemesanan penerbangan                              | Tinggi 90%                | User          |
| 81 | Menerima e-tiket dan konfirmasi penerbangan                                   | Tinggi 85%                | User          |
| 82 | Mengelola pemesanan penerbangan, seperti pengubahan atau pembatalan           | Tinggi 90%                | User          |
| 83 | Memberikan ulasan dan rating terhadap pengalaman penerbangan                  | Sedang 75%                | User          |
| 84 | Melakukan integrasi dengan sistem pihak ketiga, seperti maskapai              | Tinggi 80%                | Admin         |
| 85 | Mengelola data maskapai, jadwal penerbangan, dan harga                        | Tinggi 85%                | Admin         |
| 86 | Memantau ketersediaan dan harga penerbangan                                   | Tinggi 85%                | Admin         |
| 87 | Memberikan notifikasi perubahan jadwal atau informasi penerbangan             | Tinggi 90%                | User          |
| 88 | Mengelola informasi penumpang, seperti paspor atau visa                       | Sedang 75%                | Admin         |
| 89 | Mengakses informasi tentang bagasi, berat maksimum, dan biaya                 | Sedang 70%                | User          |
| 90 | Melakukan integrasi dengan sistem keamanan bandara                            | Tinggi 80%                | Admin         |
| 91 | Melakukan pembaruan dan peningkatan fitur aplikasi                            | Tinggi 80%                | Admin         |
| 92 | Melakukan integrasi dengan sistem keamanan bandara                            | Tinggi 80%                | Admin         |
| 93 | Pencarian destinasi wisata berdasarkan lokasi dan kategori                    | Tinggi 80%                | User          |
| 94 | Menerima tiket dan konfirmasi reservasi wisata                                | Tinggi 85%                | User          |
| 95 | Melakukan integrasi dengan sistem keamanan bandara                            | Tinggi 80%                | Admin         |
| 96 | Mengakses informasi kontak atau layanan pelanggan                             | Sedang 70%                | Admin         |
| 97 | Melakukan integrasi dengan mitra penyedia paket wisata                        | Tinggi 80%                | Admin         |
| 98 | Memantau ketersediaan dan harga paket wisata                                  | Tinggi 85%                | User          |
| 99 | Fitur pemesanan akomodasi dengan layanan rental mobil                         | Sedang 70%                | User          |
| 100 | Pencarian akomodasi dekat area alun-alun atau taman kota bandara             | Sedang 70%                | User          |

## No.2

## No.3
## Visualisasi Jumlah Pengguna per Kota
![image](download__1_.png)
```
import matplotlib.pyplot as plt
import pandas as pd

# Koneksi ke database dan eksekusi query untuk mengambil data
# (Kode koneksi ke database tidak diberikan dalam contoh ini, karena itu tergantung pada preferensi dan lingkungan Anda)

# Query untuk mengambil data
query = "SELECT kota, COUNT(*) as jumlah_pengguna FROM agoda GROUP BY kota ORDER BY jumlah_pengguna DESC;"

# Eksekusi query dan ambil hasilnya
# (Anggaplah bahwa hasil query disimpan dalam variabel df_pengguna_per_kota)
# df_pengguna_per_kota = pd.read_sql(query, conn)

# Contoh data
data = {
    'kota': ['Jakarta', 'Surabaya', 'Bandung', 'Yogyakarta', 'Malang'],
    'jumlah_pengguna': [100, 80, 70, 60, 50]
}

df_pengguna_per_kota = pd.DataFrame(data)

# Visualisasi
plt.figure(figsize=(10, 6))
plt.bar(df_pengguna_per_kota['kota'], df_pengguna_per_kota['jumlah_pengguna'], color='skyblue', edgecolor='black')
plt.title('Jumlah Pengguna per Kota', fontsize=16)
plt.xlabel('Kota', fontsize=14)
plt.ylabel('Jumlah Pengguna', fontsize=14)
plt.xticks(rotation=45, ha='right')
plt.tight_layout()
plt.show()
```

## Visualisasi Rata-rata Harga Perjalanan per Destinasi
![image](download__2_.png)
```

# Query untuk mengambil data
query = "SELECT d.nama AS destinasi, AVG(p.harga) AS rata_rata_harga FROM destinasi d INNER JOIN perjalanan p ON d.id = p.destinasi_id GROUP BY d.nama ORDER BY rata_rata_harga DESC;"

# Eksekusi query dan ambil hasilnya
# (Anggaplah bahwa hasil query disimpan dalam variabel df_rata_rata_harga_perjalanan)
# df_rata_rata_harga_perjalanan = pd.read_sql(query, conn)

# Contoh data
data = {
    'destinasi': ['Bali', 'Lombok', 'Makassar', 'Medan', 'Yogyakarta'],
    'rata_rata_harga': [2500000, 2200000, 1800000, 1600000, 1500000]
}

df_rata_rata_harga_perjalanan = pd.DataFrame(data)

# Visualisasi
plt.figure(figsize=(10, 6))
plt.bar(df_rata_rata_harga_perjalanan['destinasi'], df_rata_rata_harga_perjalanan['rata_rata_harga'], color='lightgreen', edgecolor='black')
plt.title('Rata-rata Harga Perjalanan per Destinasi', fontsize=16)
plt.xlabel('Destinasi', fontsize=14)
plt.ylabel('Rata-rata Harga', fontsize=14)
plt.xticks(rotation=45, ha='right')
plt.tight_layout()
plt.show()
```


## Visualisasi Persentase Metode Pembayaran yang Digunakan
![image](download__3_.png)
```

# Query untuk mengambil data
query = "SELECT m.nama AS metode_pembayaran, COUNT(t.id) AS jumlah_transaksi FROM metode_pembayaran m LEFT JOIN transaksi t ON m.id = t.metode_pembayaran_id GROUP BY m.nama;"

# Eksekusi query dan ambil hasilnya
# (Anggaplah bahwa hasil query disimpan dalam variabel df_jumlah_transaksi_per_metode)
# df_jumlah_transaksi_per_metode = pd.read_sql(query, conn)

# Contoh data
data = {
    'metode_pembayaran': ['Transfer Bank', 'Kartu Kredit', 'PayPal', 'Dana', 'OVO', 'GoPay', 'LinkAja', 'ShopeePay', 'Doku Wallet', 'Klarna'],
    'jumlah_transaksi': [100, 80, 70, 60, 50, 40, 30, 20, 15, 10]
}

df_jumlah_transaksi_per_metode = pd.DataFrame(data)

# Visualisasi
plt.figure(figsize=(10, 6))
plt.pie(df_jumlah_transaksi_per_metode['jumlah_transaksi'], labels=df_jumlah_transaksi_per_metode['metode_pembayaran'], autopct='%1.1f%%', startangle=140, colors=plt.cm.Paired.colors)
plt.title('Persentase Metode Pembayaran yang Digunakan', fontsize=16)
plt.axis('equal')
plt.tight_layout()
plt.show()
```

## Visualisasi Perbandingan Jumlah Transaksi Berdasarkan Status Pemesanan
![image](download__4_.png)
```

# Query untuk mengambil data
query = "SELECT status, COUNT(*) as jumlah_transaksi FROM pemesanan GROUP BY status;"

# Eksekusi query dan ambil hasilnya
# (Anggaplah bahwa hasil query disimpan dalam variabel df_jumlah_transaksi_per_status)
# df_jumlah_transaksi_per_status = pd.read_sql(query, conn)

# Contoh data
data = {
    'status': ['dipesan', 'selesai', 'dibatalkan', 'pending'],
    'jumlah_transaksi': [80, 70, 10, 5]
}

df_jumlah_transaksi_per_status = pd.DataFrame(data)

# Visualisasi
plt.figure(figsize=(10, 6))
plt.bar(df_jumlah_transaksi_per_status['status'], df_jumlah_transaksi_per_status['jumlah_transaksi'], color='coral', edgecolor='black')
plt.title('Perbandingan Jumlah Transaksi Berdasarkan Status Pemesanan', fontsize=16)
plt.xlabel('Status Pemesanan', fontsize=14)
plt.ylabel('Jumlah Transaksi', fontsize=14)
plt.tight_layout()
plt.show()
```


## Visualisasi Rating Rata-rata Pengalaman per Destinasi
![image](download__5_.png)
```
# Query untuk mengambil data
query = "SELECT d.nama AS destinasi, AVG(u.rating) AS rating_rata_rata FROM destinasi d INNER JOIN ulasan_pengalaman u ON d.id = u.pengalaman_id GROUP BY d.nama ORDER BY rating_rata_rata DESC;"

# Eksekusi query dan ambil hasilnya
# (Anggaplah bahwa hasil query disimpan dalam variabel df_rating_rata_rata_per_destinasi)
# df_rating_rata_rata_per_destinasi = pd.read_sql(query, conn)

# Contoh data
data = {
    'destinasi': ['Bali', 'Lombok', 'Jakarta', 'Yogyakarta', 'Bandung'],
    'rating_rata_rata': [4.9, 4.8, 4.7, 4.6]
}

df_rating_rata_rata_per_destinasi = pd.DataFrame(data)

# Visualisasi
plt.figure(figsize=(10, 6))
plt.bar(df_rating_rata_rata_per_destinasi['destinasi'], df_rating_rata_rata_per_destinasi['rating_rata_rata'], color='purple', edgecolor='black')
plt.title('Rating Rata-rata Pengalaman per Destinasi', fontsize=16)
plt.xlabel('Destinasi', fontsize=14)
plt.ylabel('Rating Rata-rata', fontsize=14)
plt.ylim(0, 5)  # Batas sumbu y dari 0 hingga 5
plt.xticks(rotation=45, ha='right')
plt.tight_layout()
plt.show()
```

## Visualisasi Jumlah Pemesanan per Bulan
![image](download__6_.png)
```
# Query untuk mengambil data
query = "SELECT DATE_FORMAT(tanggal_pemesanan, '%Y-%m') AS bulan, COUNT(*) AS jumlah_pemesanan FROM pemesanan GROUP BY bulan ORDER BY bulan;"

# Eksekusi query dan ambil hasilnya
# (Anggaplah bahwa hasil query disimpan dalam variabel df_jumlah_pemesanan_per_bulan)
# df_jumlah_pemesanan_per_bulan = pd.read_sql(query, conn)

# Contoh data
data = {
    'bulan': ['2021-01', '2021-02', '2021-03', '2021-04', '2021-05', '2021-06'],
    'jumlah_pemesanan': [100, 120, 90, 110, 130, 100]
}

df_jumlah_pemesanan_per_bulan = pd.DataFrame(data)

# Visualisasi
plt.figure(figsize=(10, 6))
plt.plot(df_jumlah_pemesanan_per_bulan['bulan'], df_jumlah_pemesanan_per_bulan['jumlah_pemesanan'], marker='o', color='gold')
plt.title('Jumlah Pemesanan per Bulan', fontsize=16)
plt.xlabel('Bulan', fontsize=14)
plt.ylabel('Jumlah Pemesanan', fontsize=14)
plt.xticks(rotation=45, ha='right')
plt.tight_layout()
plt.show()
```

## Visualisasi Durasi Rata-rata Perjalanan per Destinasi
![image](download__7_.png)
```
# Query untuk mengambil data
query = "SELECT d.nama AS destinasi, AVG(p.durasi) AS durasi_rata_rata FROM destinasi d INNER JOIN perjalanan p ON d.id = p.destinasi_id GROUP BY d.nama ORDER BY durasi_rata_rata DESC;"

# Eksekusi query dan ambil hasilnya
# (Anggaplah bahwa hasil query disimpan dalam variabel df_durasi_rata_rata_perjalanan)
# df_durasi_rata_rata_perjalanan = pd.read_sql(query, conn)

# Contoh data
data = {
    'destinasi': ['Bali', 'Lombok', 'Jakarta', 'Yogyakarta', 'Bandung'],
    'durasi_rata_rata': [7, 6, 5, 4, 3]
}

df_durasi_rata_rata_perjalanan = pd.DataFrame(data)

# Visualisasi
plt.figure(figsize=(10, 6))
plt.bar(df_durasi_rata_rata_perjalanan['destinasi'], df_durasi_rata_rata_perjalanan['durasi_rata_rata'], color='navy', edgecolor='black')
plt.title('Durasi Rata-rata Perjalanan per Destinasi', fontsize=16)
plt.xlabel('Destinasi', fontsize=14)
plt.ylabel('Durasi Rata-rata (Hari)', fontsize=14)
plt.xticks(rotation=45, ha='right')
plt.tight_layout()
plt.show()
```

## No.4
## SELECT COUNT()
Menggunakan fungsi COUNT() untuk menghitung jumlah baris dalam tabel agoda.
```
SELECT COUNT(*) AS total_pengguna FROM agoda;
```

**Hasil :**
| total_pengguna |
| -- |
|  10 |

## SELECT AVG()
Menggunakan fungsi AVG() untuk menghitung rata-rata harga dari semua perjalanan dalam tabel perjalanan.
```
SELECT AVG(harga) AS rata_rata_harga FROM perjalanan;
```

**Hasil :**
| rata_rata_harga |
| -- |
|  1650000.00 |

## SELECT CONCAT()
Menggunakan fungsi CONCAT() untuk menggabungkan kolom nama dan kota dalam tabel agoda menjadi satu kolom nama_kota.
```
SELECT CONCAT(nama, ' - ', kota) AS nama_kota FROM agoda;
```

**Hasil :**
| nama_kota |
| -- |
|  Tatang Sampurna - Jakarta |
|  Supomo Darma - Bandung |
|  Michael Arteta - Surabaya |
|  Sarah Arani - Yogyakarta |
|  Rani Chelseana - Malang |
|  Emil Kamil - Bandung |
|  Michael Jordan - Surabaya |
|  Sophia Taslim - Yogyakarta |
|  Aldrian Rizki - Jakarta |
|  Lionel Messi - Malang |

## No.5
Subquery (juga sering disebut sebagai subkueri) adalah suatu kueri yang tertanam di dalam kueri utama. Fungsinya adalah untuk mendapatkan data yang lebih spesifik atau melakukan perhitungan yang kompleks dengan menggunakan data dari tabel yang terkait.

Penggunaan subquery umumnya terjadi dalam pernyataan SELECT, INSERT, UPDATE, atau DELETE untuk melakukan filter, penggabungan, atau perhitungan data dengan cara yang lebih kompleks. Subquery biasanya ditempatkan di dalam klausa WHERE, FROM, atau HAVING dari kueri utama.

Salah satu contoh penggunaan subquery ini adalah untuk menampilkan jumlah total pemesanan pada tabel perjalanan (perjalanan) berdasarkan perjalanan tertentu. Misalnya, kita ingin mengetahui berapa banyak pemesanan yang telah dilakukan untuk perjalanan dengan id 1 (Liburan Bali).

```
SELECT
  nama,
  (SELECT COUNT(*) FROM pemesanan WHERE perjalanan_id = 1) AS jumlah_pemesanan
FROM perjalanan
WHERE id = 1;
```
Subquery di dalam SELECT digunakan untuk menghitung jumlah pemesanan pada perjalanan dengan id 1. Hasilnya akan menampilkan nama perjalanan (Liburan Bali) beserta jumlah pemesanan yang dilakukan untuk perjalanan tersebut.

**Hasilnya akan seperti ini:**

**Hasil :**
| nama | jumlah_pemesanan |
| -- | ------ |
|  Liburan Bali | 2 |


Contoh lain penggunaan subquery adalah untuk menampilkan nama pengguna (agoda) dan total harga pemesanan (total harga dari seluruh perjalanan yang dipesan oleh pengguna tersebut). Kita juga dapat menggunakan fungsi agregasi SUM untuk menghitung total harga.

```
SELECT
  nama,
  (SELECT SUM(harga) FROM perjalanan WHERE id IN (SELECT perjalanan_id FROM pemesanan WHERE user_id = agoda.id)) AS total_harga
FROM agoda;

```
Subquery di dalam SELECT digunakan untuk menghitung total harga perjalanan yang dipesan oleh setiap pengguna. Subquery pertama akan mengambil daftar id perjalanan dari tabel pemesanan berdasarkan user_id di tabel agoda. Subquery kedua akan menghitung total harga perjalanan dari daftar id perjalanan tersebut.

Hasilnya akan menampilkan nama pengguna (agoda) beserta total harga dari seluruh perjalanan yang dipesan oleh pengguna tersebut.

| nama | total_harga |
| -- | ------ |
|  Tatang Sampurna | 4000000.00  |
|  Supomo Darma | 4500000.00   |
|  Michael Arteta | 6000000.00  |
|  Sarah Arani | 5000000.00  |
|  Rani Chelseana | 3600000.00  |
|  Emil Kamil  | 2800000.00  |
|  Michael Jordan | 4000000.00   |
|  Sophia Taslim  | 4200000.00  |
|  Aldrian Rizki | 5400000.00  |
|  Lionel Messi | 5800000.00  |

## No.6

Penggunaan transaksi dalam konteks produk digital dapat membantu memastikan integritas dan konsistensi data saat melakukan operasi yang melibatkan beberapa tabel sekaligus. Transaksi memastikan bahwa operasi-operasi tersebut entah berhasil dilaksanakan sepenuhnya atau tidak dilaksanakan sama sekali jika ada kesalahan atau kegagalan selama proses.

Dalam contoh skema tabel, kita dapat mengasumsikan bahwa suatu pengguna ingin memesan perjalanan dan menginap di sebuah destinasi. Proses pemesanan melibatkan tabel-tabel seperti pemesanan, perjalanan, penginapan, kamar, dan transaksi. Jika salah satu operasi gagal (misalnya, pembayaran gagal atau kamar penuh), transaksi akan memastikan bahwa semua operasi yang telah dilakukan dibatalkan, sehingga tidak ada data yang tersisa dalam keadaan setengah selesai.

contoh sederhana penggunaan transaksi dalam skenario ini :

```
BEGIN TRANSACTION;

-- Operasi 1: Memasukkan data pemesanan
INSERT INTO pemesanan (id, user_id, perjalanan_id, tanggal_pemesanan, status, jumlah_orang)
VALUES (11, 1, 1, '2023-08-15', 'dipesan', 2);

-- Operasi 2: Mengurangi kapasitas kamar yang dipesan
UPDATE kamar
SET kapasitas = kapasitas - 2
WHERE id = 1 AND kapasitas >= 2;

-- Operasi 3: Memasukkan data transaksi
INSERT INTO transaksi (id, user_id, perjalanan_id, kamar_id, jumlah_kamar, metode_pembayaran_id, tanggal_transaksi, total_bayar)
VALUES (11, 1, 1, 1, 2, 2, '2023-08-15', 3000000);

-- Commit transaksi jika semua operasi berhasil
COMMIT;
```

Dalam contoh di atas, jika ada masalah dalam salah satu operasi (misalnya, kapasitas kamar tidak mencukupi), transaksi akan otomatis dibatalkan dengan memanggil ROLLBACK, sehingga tidak ada perubahan data yang akan disimpan dalam database.

Penggunaan transaksi membantu memastikan bahwa data tetap konsisten dan akurat dalam sistem, bahkan ketika ada situasi yang tidak terduga atau kesalahan dalam proses operasi. Dengan demikian, pengguna dapat mempercayai sistem dalam melakukan transaksi dan menghindari konsekuensi dari data yang tidak konsisten atau tidak lengkap.


## No.7
**Procedure / Function**

Sebuah Procedure atau Function adalah kumpulan instruksi SQL yang diberi nama dan digunakan untuk melakukan tugas tertentu dalam database. Mereka membantu mengorganisir dan mengelompokkan beberapa perintah SQL sehingga dapat dipanggil sebagai satu entitas tunggal.

Dalam konteks produk digital saya, saya akan membuat dua contoh Procedure dan Function:

**Contoh Procedure: Menambahkan Pengalaman Baru**

Misalkan kita ingin membuat sebuah Procedure untuk menambahkan pengalaman baru ke tabel "pengalaman". Berikut adalah contoh untuk Procedure ini:

```
CREATE PROCEDURE tambah_pengalaman(
  IN nama_pengalaman VARCHAR(255),
  IN deskripsi_pengalaman VARCHAR(255),
  IN harga_pengalaman DECIMAL(15, 2),
  IN kapasitas_pengalaman INT,
  IN foto_pengalaman VARCHAR(255),
  IN user_id INT,
  IN destinasi_id INT
)
BEGIN
  INSERT INTO pengalaman (nama, deskripsi, harga, kapasitas, foto, user_id, destinasi_id)
  VALUES (nama_pengalaman, deskripsi_pengalaman, harga_pengalaman, kapasitas_pengalaman, foto_pengalaman, user_id, destinasi_id);
END;
```

Penjelasan:

Procedure ini bernama "tambah_pengalaman" dengan 7 parameter masukan (IN).
Ketika Procedure dipanggil, data yang diberikan untuk setiap parameter akan dimasukkan ke dalam tabel "pengalaman".

**Cara Menggunakan Procedure:**

Misalkan kita ingin menambahkan pengalaman baru dengan data berikut:

Nama: "Pengalaman Baru di Bali"
Deskripsi: "Nikmati Sunset di Pantai Kuta"
Harga: 300000
Kapasitas: 15
Foto: "pengalaman_bali.jpg"
User ID: 1
Destinasi ID: 1

Kita bisa menggunakan Procedure dengan perintah berikut:
```
CALL tambah_pengalaman('Pengalaman Baru di Bali', 'Nikmati Sunset di Pantai Kuta', 300000, 15, 'pengalaman_bali.jpg', 1, 1);
```

**Contoh Function: Total Pemesanan Pengguna**

Misalkan kita ingin membuat sebuah Function yang menghitung total pemesanan pengguna berdasarkan ID pengguna yang diberikan. Berikut adalah contoh untuk Function ini:
```
CREATE FUNCTION total_pemesanan(user_id INT) RETURNS INT
BEGIN
  DECLARE total_pesanan INT;
  SELECT SUM(jumlah_orang) INTO total_pesanan FROM pemesanan WHERE user_id = user_id;
  RETURN total_pesanan;
END;
```
Penjelasan:

Function ini bernama "total_pemesanan" dengan 1 parameter masukan (user_id).
Function ini mengambil total pesanan (jumlah orang) dari tabel "pemesanan" berdasarkan user_id yang diberikan.

**Cara Menggunakan Function:**

Misalkan kita ingin menghitung total pesanan untuk pengguna dengan ID 1. Kita bisa menggunakan Function dengan perintah berikut:
```
SELECT total_pemesanan(1);
```
Function ini akan mengembalikan nilai total pesanan untuk pengguna dengan ID 1.

**Trigger**
Sebuah Trigger adalah mekanisme dalam database yang memungkinkan tindakan atau prosedur otomatis terjadi ketika ada perubahan data dalam tabel tertentu. Dalam kasus produk digital di atas, mari buat contoh Trigger yang akan otomatis mengurangi kapasitas penginapan setiap kali pemesanan baru dibuat.

**Contoh Trigger: Mengurangi Kapasitas Penginapan**

Berikut adalah contoh SQL untuk Trigger ini:
```
CREATE TRIGGER kurangi_kapasitas_penginapan AFTER INSERT ON pemesanan
FOR EACH ROW
BEGIN
  UPDATE penginapan SET kapasitas = kapasitas - NEW.jumlah_orang
  WHERE penginapan.id = NEW.kamar_id;
END;
```
Penjelasan:

Trigger ini bernama "kurangi_kapasitas_penginapan".
Trigger ini diaktifkan setiap kali ada data baru yang dimasukkan ke dalam tabel "pemesanan" (AFTER INSERT).
Setiap kali pemesanan baru dibuat, kapasitas penginapan akan dikurangi sesuai jumlah orang yang dipesan.

## No.8

Berikut ini adalah demonstrasi penggunaan Data Control Language (DCL) pada skema tabel :

**GRANT:** Memberikan izin akses kepada pengguna tertentu.
```
-- Contoh: Memberikan izin SELECT pada tabel pemesanan kepada user dengan id=1

GRANT SELECT ON pemesanan TO user_id=1;

```
**REVOKE:** Mencabut izin akses dari pengguna tertentu.
```
-- Contoh: Mencabut izin SELECT pada tabel penginapan dari user dengan id=2

REVOKE SELECT ON penginapan FROM user_id=2;

```


**COMMIT:** Menyimpan perubahan data yang dilakukan dalam sesi transaksi saat ini secara permanen.
```
-- Contoh: Menyimpan perubahan data setelah melakukan beberapa operasi INSERT, UPDATE, atau DELETE

COMMIT;
```

**ROLLBACK:** Membatalkan perubahan data dalam sesi transaksi saat ini sebelum di-Commit.
```
-- Contoh: Membatalkan perubahan data setelah melakukan beberapa operasi INSERT, UPDATE, atau DELETE

ROLLBACK;
```

**SAVEPOINT:** Membuat titik pemulihan dalam sesi transaksi.
```
-- Contoh: Membuat titik pemulihan untuk kemudian bisa melakukan ROLLBACK ke titik ini jika diperlukan

SAVEPOINT sp1;
```

**GRANT OPTION:** Mengizinkan pengguna untuk memberikan izin akses kepada pengguna lain.
```
-- Contoh: Memberikan izin SELECT dengan GRANT OPTION pada tabel ulasan kepada user dengan id=3

GRANT SELECT ON ulasan TO user_id=3 WITH GRANT OPTION;
```

**WITH ADMIN OPTION:** Memberikan izin ADMIN kepada pengguna untuk mengelola hak akses.

```
-- Contoh: Memberikan izin INSERT, UPDATE, DELETE pada tabel penginapan kepada user dengan id=4 dengan ADMIN OPTION

GRANT INSERT, UPDATE, DELETE ON penginapan TO user_id=4 WITH ADMIN OPTION;
```

## No.9
## Constraint PRIMARY KEY:
Constraint ini digunakan untuk menetapkan satu atau beberapa kolom sebagai kunci utama pada suatu tabel. Kunci utama ini digunakan untuk mengidentifikasi secara unik setiap baris dalam tabel. Pada contoh di atas, kolom id pada setiap tabel yang memiliki constraint PRIMARY KEY berperan sebagai kunci utama.

Contoh:
```
CREATE TABLE agoda (
  id INT PRIMARY KEY,
  ...
);
```

##Constraint UNIQUE: 
Constraint ini digunakan untuk memastikan bahwa nilai-nilai di dalam kolom yang memiliki constraint UNIQUE adalah unik, sehingga tidak ada duplikasi data dalam kolom tersebut. Pada contoh di atas, kolom email pada tabel agoda memiliki constraint UNIQUE, yang artinya tidak ada dua akun dengan alamat email yang sama.

Contoh:
```
CREATE TABLE agoda (
  ...
  email VARCHAR(255) UNIQUE,
  ...
);
```

## Constraint NOT NULL: 
Constraint ini digunakan untuk memastikan bahwa nilai di dalam kolom tidak boleh kosong (NULL). Pada contoh di atas, kolom nama pada tabel destinasi dan kolom mulai_tanggal pada tabel perjalanan menggunakan constraint NOT NULL, sehingga nilai-nilai di kolom tersebut harus diisi.

Contoh:
```
CREATE TABLE destinasi (
  id INT PRIMARY KEY,
  nama VARCHAR(255) NOT NULL,
  ...
);

CREATE TABLE perjalanan (
  id INT PRIMARY KEY,
  nama VARCHAR(255) NOT NULL,
  destinasi_id INT,
  mulai_tanggal DATE NOT NULL,
  berakhir_tanggal DATE,
  ...
);
```

## Constraint FOREIGN KEY: 
Constraint ini digunakan untuk membangun hubungan antara dua tabel. Kolom yang memiliki constraint FOREIGN KEY berperan sebagai referensi ke kolom di tabel lain yang memiliki constraint PRIMARY KEY. Dengan constraint ini, dapat dilakukan operasi JOIN antara dua tabel. Pada contoh di atas, kolom destinasi_id pada tabel perjalanan adalah FOREIGN KEY yang merujuk ke kolom id pada tabel destinasi.

Contoh:
```
CREATE TABLE perjalanan (
  ...
  destinasi_id INT,
  FOREIGN KEY (destinasi_id) REFERENCES destinasi(id)
);
```

## Constraint DEFAULT: 
Constraint ini digunakan untuk memberikan nilai default pada suatu kolom jika nilai tidak diisi secara eksplisit saat melakukan operasi INSERT. Pada contoh di atas, kolom status pada tabel pemesanan memiliki constraint DEFAULT yang diberi nilai 'dipesan', sehingga jika tidak ada nilai yang diberikan saat melakukan pemesanan, maka status pemesanan akan secara otomatis diisi dengan 'dipesan'.

Contoh:
```
CREATE TABLE pemesanan (
  ...
  status VARCHAR(255) DEFAULT 'dipesan',
  ...
);
```

## Constraint CHECK: 
Constraint ini digunakan untuk menentukan suatu kondisi yang harus dipenuhi oleh nilai di dalam kolom. Jika kondisi tidak dipenuhi, maka operasi INSERT atau UPDATE akan gagal. Pada contoh di atas, tidak ada contoh penggunaan constraint CHECK, tetapi bisa ditambahkan jika ada kebutuhan khusus untuk mengatur batasan nilai pada kolom tertentu.

Contoh (simulasi penggunaan constraint CHECK):
```
CREATE TABLE produk (
  ...
  stok INT CHECK (stok >= 0),
  ...
);
```

## Constraint CASCADE:
Constraint ini digunakan dalam hubungan referensial antara tabel. Jika sebuah baris di tabel induk dihapus atau diperbarui, maka baris-baris yang terhubung melalui FOREIGN KEY di tabel anak akan dihapus atau diperbarui secara otomatis sesuai kebijakan yang ditentukan (CASCADE).

Contoh (simulasi penggunaan constraint CASCADE saat menghapus baris di tabel induk):
```
CREATE TABLE parent (
  id INT PRIMARY KEY
);

CREATE TABLE child (
  id INT PRIMARY KEY,
  parent_id INT,
  FOREIGN KEY (parent_id) REFERENCES parent(id) ON DELETE CASCADE
);

-- Jika baris dengan id=1 di tabel parent dihapus, maka baris-baris yang memiliki parent_id=1 di tabel child juga akan dihapus.
```

## No.10

## No.11
